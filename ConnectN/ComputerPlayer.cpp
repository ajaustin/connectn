// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  ComputerPlayer.cpp
//
//  This file defines a class that represents a computer player for
//  a game of Connect N. The class contains functions for getting
//  the computer's move and setting and getting the computer's piece
//  color

#include <limits>
#include <algorithm>

#include "ComputerPlayer.h"
#include "Board.h"


// Function is a default constructor for the ComputerPlayer. THe
// function initializes the color of the computer's piece to EMPTY
// and sets the computer's strategy and difficulty to defensive and
// medium, respectively
//      Parameters:         none
//      Returns:            none
//      Postcondition:      computer's color set to EMPTY, difficulty
//                          to medium, and strategy to defensive
ComputerPlayer::ComputerPlayer()
{
    color = EMPTY;

    setDifficulty(MEDIUM);
    setStrategy(DEFENSIVE);
}


// Function gets the color of the computer's piece.
//      Parameters:         none
//      Returns:            piece representing computer's color
piece ComputerPlayer::getColor()
{
    return color;
}


// Function sets the computer's color to the passed piece. Also
// sets color of opponent's piece
//      Parameters:         piece p: color to set as
//                          computer's piece
//      Returns:            none
//      Postcondition:      computer's and oponent's colors set
void ComputerPlayer::setColor(piece p)
{
    color = p;

    opponent = (p == RED) ? BLACK : RED;
}


// Function gets the computer's strategy.
//      Parameters:         none
//      Returns:            computer's strategy
strategy ComputerPlayer::getStrategy()
{
    return strat;
}


// Function sets the computer's strategy to the passed strategy.
//      Parameters:         strategy s: computer's strategy
//      Returns:            none
//      Postcondition:      computer's strategy set
void ComputerPlayer::setStrategy(strategy s)
{
    strat = s;

    switch (s) {
    case OFFENSIVE:
        myThreatCoeff = 6;
        oppThreatCoeff = 7;
        break;
    case DEFENSIVE:
        myThreatCoeff = 2;
        oppThreatCoeff = 3;
        break;
    default:
        myThreatCoeff = 1;
        oppThreatCoeff = 1;
    }
}


// Function gets the computer's difficulty mode.
//      Parameters:         none
//      Returns:            computer's difficulty mode
mode ComputerPlayer::getDifficulty()
{
    return difficulty;
}


// Function sets the computer's difficulty to the passed
// difficulty.
//      Parameters:         mode m: difficulty of computer
//      Returns:            none
//      Postcondition:      computer's difficulty set
void ComputerPlayer::setDifficulty(mode m)
{
    difficulty = m;

    switch (m) {
    case EASY:
        depth = EASY_DEPTH;
        break;
    case MEDIUM:
        depth = MED_DEPTH;
        break;
    default:
        depth = HARD_DEPTH;
    }
}


// Function sets the computer's board state. This function
// should be called at the beginning of the computer's turn.
//      Parameters:         Board &board: board state
//      Returns:            none
//      Postcondition:      board state set
void ComputerPlayer::setBoardState(const Board& board)
{
    state = board;
}


// Function gets the computer's move by searching for the best
// move it can make given the current board state. The computer
// looks ahead a specified number of moves and evaluates the
// board once those moves have been made. THe board with the
// best "score" is the move the comptuer will take.
//      Parameters:         none
//      Returns:            int move: computer's chosen move
int ComputerPlayer::getMove()
{
    int r;                          // row to make move in

    int bestValue = std::numeric_limits<int>::min();    // start value at -inf
    int currentValue;                                   // current move value

    std::vector<int> moves = getMoves(state);

    int bestColumn = moves[0];

    // try all possible column moves
    for (auto & c : moves) {
        r = state.placePiece(c, color);

        // if an immediate win, return that move
        if (state.checkWin(r, c, color)) {
            state.unplacePiece(c);
            return c;
        }

        // ...otherwise, begin search
        currentValue = minimax(1, false, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
        //std::cout << "column " << c << " value : " << currentValue << std::endl;
            
        // if needed, update best value and best column
        if (currentValue > bestValue) {
            bestColumn = c;
            bestValue = currentValue;
        }

        state.unplacePiece(c);
    }

    //std::cout << "I moved in column " << bestColumn << std::endl;

    return bestColumn;
}


// Function uses the minimax algorithm to look ahead a certain number
// of moves to help the computer determine which move to make. If
// the maximum search depth has been reached or if there are no
// possible moves left, evaluates the current board state.
//      Parameters:     int currDepth: current search depth
//                      bool maximizing: whether the current
//                      player is the maximizing player
//                      int alpha: alpha value for alpha-beta pruning
//                      int beta: beta value for alpha-beta pruning
//      Returns:        value of the best column move
int ComputerPlayer::minimax(int currDepth, bool maximizing, int alpha, int beta)
{
    int r;              // row player moves in

    // base case: max depth reached or board is full
    if (currDepth >= depth || state.isFull()) {
        return evaluateBoard();
    }

    if (maximizing) {
        // computer's turn
        int bestValue = std::numeric_limits<int>::min();

        // try all possible moves
        for (int c = 0; c < state.getColumns(); c++) {
            if (state.validMove(c)) {
                r = state.placePiece(c, color);

                // if move results in a win, return +inf
                if (state.checkWin(r, c, color)) {
                    state.unplacePiece(c);
                    return 100000;
                }

                // ...otherwise, continue search
                int currentValue = minimax(currDepth + 1, false, alpha, beta);
                state.unplacePiece(c);

                // update bestValue if necessary
                if (currentValue > bestValue) {
                    bestValue = currentValue;
                }

                if (currentValue > alpha) {
                    alpha = currentValue;
                }

                if (beta <= alpha) {
                    break;
                }
            }
        }
        return bestValue;
    }
    else {
        // opponent's turn
        int bestValue = std::numeric_limits<int>::max();

        // try all possible moves
        for (int c = 0; c < state.getColumns(); c++) {
            if (state.validMove(c)) {
                r = state.placePiece(c, opponent);

                // if move results in a loss for computer,
                // return -inf
                if (state.checkWin(r, c, opponent)) {
                    state.unplacePiece(c);
                    return -100000;
                }

                // ...otherwise, continue search
                int currentValue = minimax(currDepth + 1, true, alpha, beta);
                state.unplacePiece(c);

                // update best value if necessary
                if (currentValue < bestValue) {
                    bestValue = currentValue;
                }

                if (beta > bestValue) {
                    beta = bestValue;
                }

                if (beta <= alpha) {
                    break;
                }

            }
        }
        return bestValue;
    }
}


// Function evaluates the current board state for the computer.
// Evaluation function is:
//      myCoefficient * myThreats - oppCoefficient * oppThreats
// A threat is defined as having N-1 unblocked pieces in a row
// allowing for gaps.
//      Parameters:     none
//      Returns:        evaluation score for board state
int ComputerPlayer::evaluateBoard()
{
    int score = 0;

    score += myThreatCoeff * countThreats(color);
    score -= oppThreatCoeff * countThreats(opponent);

    return score;
}


// Function counts the number of threats the player with
// the passed piece has. Threats are defined as having
// N - 1 unblocked pieces in a row, allowing for gaps.
// For example, x_xx is counted as a horizontal threat.
// Gaps are allowed at the beginning and end of threats,
// so _xxx_ will count as two distinct threats. To count
// as a threat, a player must be able to eventually place
// a piece to complete the threat. For example, oxxx_ is
// considered a valid threat, but oxxxo is not.
//      Parameters:     piece p: color to count threats of
//      Returns:        number of threats player has
int ComputerPlayer::countThreats(piece p)
{
    int count = 0;

    // find out which piece is opponent's color
    piece opposite = (p == RED) ? BLACK : RED;

    // search all cells
    for (int r = 0; r < state.getRows(); r++) {
        for (int c = 0; c < state.getColumns(); c++) {
            // see if threat starts at (r, c) (a thrat can
            // start at (r, c) as long as the piece at (r, c)
            // is not controlled by the opponent)
            if (state.getCell(r, c) != opposite) {
                count += checkThreatHorizontal(r, c, p);
                count += checkThreatVertical(r, c, p);
                count += checkThreatPosDiag(r, c, p);
                count += checkThreatNegDiag(r, c, p);
            }
        }
    }

    return count;
}


// Function checks if a horizontal threat starts at the
// cell (r, c) for the player with piece p.
//      Parameters:     int r: row of cell to start check
//                      int c: column of cell to start check
//                      piece p: player to check
//      Returns:        1 if a threat starts at (r, c); 0 otherwise
int ComputerPlayer::checkThreatHorizontal(int r, int c, piece p)
{
    int streak = 0;                 // number of pieces in a row
    bool gapEncountered = false;    // whether a gap has been encountered

    for (int ic = c; ic < state.getColumns(); ic++) {
        if (state.getCell(r, ic) == p) {
            // piece is controled by player
            streak++;
        }
        else if (state.getCell(r, ic) == EMPTY && !gapEncountered) {
            // piece is empty, and this is first gap encountered
            gapEncountered = true;
            streak++;
        }
        else {
            // either piece was not controled by player or 
            // a second gap was encountered; stop
            break;
        }
    }

    if (streak >= state.getN()) {
        return 1;
    }
    else {
        return 0;
    }
}


// Function checks if a vertical threat starts at the
// cell (r, c) for the player with piece p.
//      Parameters:     int r: row of cell to start check
//                      int c: column of cell to start check
//                      piece p: player to check
//      Returns:        1 if a threat starts at (r, c); 0 otherwise
int ComputerPlayer::checkThreatVertical(int r, int c, piece p)
{
    int streak = 0;                 // number of pieces in a row
    bool gapEncountered = false;    // whether a gap has been encountered

    for (int ir = r; ir >= 0; ir--) {
        if (state.getCell(ir, c) == p) {
            // piece is controled by player
            streak++;
        }
        else if (state.getCell(ir, c) == EMPTY && !gapEncountered) {
            // piece is empty, and this is first gap encountered
            gapEncountered = true;
            streak++;
        }
        else {
            // either piece was not controled by player or 
            // a second gap was encountered; stop
            break;
        }
    }

    if (streak >= state.getN()) {
        return 1;
    }
    else {
        return 0;
    }
}


// Function checks if a horizontal threat starts at the
// cell (r, c) for the player with piece p.
//      Parameters:     int r: row of cell to start check
//                      int c: column of cell to start check
//                      piece p: player to check
//      Returns:        1 if a threat starts at (r, c); 0 otherwise
int ComputerPlayer::checkThreatPosDiag(int r, int c, piece p)
{
    int streak = 0;                 // number of pieces in a row
    bool gapEncountered = false;    // whether a gap has been encountered

    for (int ic = c, ir = r; ic < state.getColumns() && ir < state.getRows(); ic++, ir++) {
        if (state.getCell(ir, ic) == p) {
            // piece is controled by player
            streak++;
        }
        else if (state.getCell(ir, ic) == EMPTY && !gapEncountered) {
            // piece is empty, and this is first gap encountered
            gapEncountered = true;
            streak++;
        }
        else {
            // either piece was not controled by player or 
            // a second gap was encountered; stop
            break;
        }
    }

    if (streak >= state.getN()) {
        return 1;
    }
    else {
        return 0;
    }
}


// Function checks if a horizontal threat starts at the
// cell (r, c) for the player with piece p.
//      Parameters:     int r: row of cell to start check
//                      int c: column of cell to start check
//                      piece p: player to check
//      Returns:        1 if a threat starts at (r, c); 0 otherwise
int ComputerPlayer::checkThreatNegDiag(int r, int c, piece p)
{
    int streak = 0;                 // number of pieces in a row
    bool gapEncountered = false;    // whether a gap has been encountered

    for (int ic = c, ir = r; ic >= 0 && ir < state.getRows(); ic--, ir++) {
        if (state.getCell(ir, ic) == p) {
            // piece is controled by player
            streak++;
        }
        else if (state.getCell(ir, ic) == EMPTY && !gapEncountered) {
            // piece is empty, and this is first gap encountered
            gapEncountered = true;
            streak++;
        }
        else {
            // either piece was not controled by player or 
            // a second gap was encountered; stop
            break;
        }
    }

    if (streak >= state.getN()) {
        return 1;
    }
    else {
        return 0;
    }
}


// Function returns a shuffled list of all possible children of a given
// state. List is shuffled to allow for variety in move selections in
// the early parts of a game where the board is large and/or the number
// of pieces in a row needed to win is large.
//      Parameters:     const Board& state: the board state to generate
//                      possible children of
//      Returns:        a vector of ints representing the possible
//                      children of a board state
std::vector<int> ComputerPlayer::getMoves(const Board& state)
{
    std::vector<int> ret;

    for (int c = 0; c < state.getColumns(); c++) {
        if (state.validMove(c)) {
            ret.push_back(c);
        }
    }

    random_shuffle(ret.begin(), ret.end());

    return ret;
}