// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  Board.cpp
//
//  This file implements a class for the board in a Connect-N game.
//  The class contains functions for setting the size of the
//  board and the N-value to connect. Class also performs
//  functions related to playing a game of Connect-N like
//  placing a piece, determining if a move is valid, and checking
//  for a win. The class also uses SFML to display graphics for the
//  board.


#include "Board.h"


// Function is the default constructor. This function initializes the
// class variables. rows, columns, and n are all set to defaults of
// 6, 7, and 4, respectively. The class texture images are loaded from
// their files.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      class variables initialized and textures
//                          loaded
Board::Board()
{
    rows = 6;
    columns = 7;
    n = 4;

    // load textures
    if (!empty.loadFromFile("resources/graphics/empty.png")) {
        std::cout << "Error loading texture image." << std::endl;
    }

    if (!red.loadFromFile("resources/graphics/red.png")) {
        std::cout << "Error loading texture image." << std::endl;
    }

    if (!black.loadFromFile("resources/graphics/black.png")) {
        std::cout << "Error loading texture image." << std::endl;
    }
}


// Function sets the N value for the Connect-N board. The N value must
// be set after the board size has been set. The N value is checked
// to ensure that it is in the allowed range and then checked to ensure
// that it is not greater than the number of rows and columns.
//      Parameterss:        int m: value to set as the N value
//      Returns:            true if N value was set, false if
//                          an error occured (out of range,
//                          larger than rows/columns, etc.)
//      Precondition:       rows and columns set before this
//                          function is called
//      Postcondition:      N value set to passed value if it is
//                          valid
bool Board::setN(int m)
{
    // check range
    if (m >= MIN_N && m <= MAX_N) {
        // check against rows and columns
        if (m <= rows && m <= columns) {
            n = m;
            return true;
        }
        else {
            std::cout << "setN: Error, N value must not be greater than number of rows and columns." << std::endl;
            return false;
        }
    }
    else {
        std::cout << "setN: Error, N value out of range." << std::endl;
        return false;
    }
}


// Function sets the size of the board (i.e., the nuber of rows and
// columns). The rows and columns values (if they are to be changed from
// the defaults) must be set before the N value. The rows and columns
// values are checked to ensure they are within the allowed range. This
// function calls the private createBoard() function to create the board.
//      Parameters:         int r: value to set as number of rows
//                          int c: value to set as number of columns
//      Returns:            true if rows and columns values were set,
//                          false if an error occured (out of range,
//                          etc.)
//      Postcondition:      rows and columns values set to passed values
//                          if they were valid
bool Board::setSize(int r, int c)
{
    // check rows range
    if (r >= MIN_ROWS && r <= MAX_ROWS) {
        // check columns range
        if (c >= MIN_COLUMNS && c <= MAX_COLUMNS) {
            rows = r;
            columns = c;
            createBoard();
            return true;
        }
        else {
            std::cout << "setSize: Error, column value out of range." << std::endl;
            return false;
        }
    }
    else {
        std::cout << "setSize: Error, row value out of range." << std::endl;
        return false;
    }
}


// Function returns the N-value for the Connect-N board.
//      Parameters:         none
//      Returns:            n (int): the number of pieces that must be
//                          connected for a win
int Board::getN() const
{
    return n;
}


// Function returns the number of rows on the board.
//      Parameters:         none
//      Returns:            rows (int): the number of rows on the board
int Board::getRows() const
{
    return rows;
}


// Function returns the number of columns on the board.
//      Parameters:         none
//      Returns:            columns (int): the number of columns on the
//                          board
int Board::getColumns() const
{
    return columns;
}


// Function returns (by reference) the number of rows and columns on the
// board.
//      Parameters:         int &r: the value to store the number of
//                          rows
//                          int &c: the value to store the number of
//                          columns
//      Returns:            rows (int) and columns (int) by reference
void Board::getSize(int& r, int&c) const
{
    r = rows;
    c = columns;
}


// Function returns the piece collor at the passed position.
//      Parameters:         int r: row to get
//                          int c: column to get
//      Returns:            piece color at passed (r, c). returns
//                          INVALID if position is invalid
piece Board::getCell(int r, int c) const
{
    if (r > -1 && r < rows && c > -1 && c < columns) {
        return vBoard[r][c];
    }
    else {
        std::cout << "Board::getCell: Error, invalid board location." << std::endl;
        return INVALID;
    }
}


// Function checks if the passed (r, c) position created a win situation
// for the player represented by the passed piece. This function calls
// the private functions to check for a win horizontally, vertically,
// and diagonally.
//      Parameters:         int r: the value that represents the row
//                          of the last move
//                          int c: the value that represents the
//                          column of the last move
//                          piece p: the player to check for a win
//      Returns:            true if the passed move created a win
//                          situation for the player represented
//                          by piece p; returns false if position
//                          is invalid or if a win is checked for
//                          an empty piece
bool Board::checkWin(int r, int c, piece p) const
{
    if (p == EMPTY) {
        std::cout << "Board::checkWin: Error, cannot check win for empty piece." << std::endl;
        return false;
    }

    if (r > -1 && r < rows && c > -1 && c < columns) {
        return checkWinHorizontal(r, c, p) 
            || checkWinVertical(r, c, p)
            || checkWinPosDiag(r, c, p) 
            || checkWinNegDiag(r, c, p);
    }

    std::cout << "Board::CheckWin: Error, invalid board location." << std::endl;
    return false;
}


// Function places a piece at the passed column. The function searches the
// column from the bottom row up in order to place the passed piece. The
// passed piece is placed at the first EMPTY position from the bottom of
// the passed column. The row it was placed on is returned for convenience.
// This function calls the public validMove() function to ensure that the
// move is valid. If the move is invalid, a -1 is returned.
//      Parameters:         int c: the column to place the piece in
//                          piece p: the player making the move
//      Returns:            r (int) row piece was placed in; -1 if
//                          column represents an invalid move
int Board::placePiece(int c, piece p)
{
    // only place if move is valid
    if (validMove(c)) {
        // start from bottom, find first empty space
        for (int r = rows - 1; r >= 0; r--) {
            if (vBoard[r][c] == EMPTY) {
                // place in empty place
                vBoard[r][c] = p;

                // update sprite
                if (p == RED) {
                    boardSprite[r][c].setTexture(red);
                }
                else {
                    boardSprite[r][c].setTexture(black);
                }

                return r;
            }
        }
    }
    std::cout << "Board::placePiece: Error, invalid move." << std::endl;
    return -1;
}


// Function unplaces the piece at the top of the passed column. If
// passed column is invalid or if passed column is empty, does nothing.
//      Parameters:         int c: the column to remove a piece
//                          from
//      Returns:            none
//      Postcondition:      topmost piece in passed column is
//                          set back to empty.
void Board::unplacePiece(int c)
{
    if (c > -1 && c < columns) {
        for (int r = 0; r < rows; r++) {
            if (vBoard[r][c] != EMPTY) {
                vBoard[r][c] = EMPTY;
                boardSprite[r][c].setTexture(empty);
                return;
            }
        }
    }
    else {
        std::cout << "Board::unplacePiece: Error, invalid column." << std::endl;
    }
}

// Function checks if a piece can be placed in the passed column. The column
// is checked at row 0. If row 0 is EMPTY, than at least one row remains to
// be filled in that column (since the column is filled from the bottom up).
//      Parameters:         int c: the column to check if a move can be
//                          made
//      Returns:            true if a move can be made in that column,
//                          false otherwise. If the passed column is
//                          invalid, a false is returned.
bool Board::validMove(int c) const
{
    if (c >= columns || c < 0) {
        std::cout << "Board::validMove: Error, invalid column." << std::endl;
        return false;
    }

    return vBoard[0][c] == EMPTY;
}


// Function checks if the board is full. This function can be used to
// determine if there is a tie since the game should end before the
// board is full if a player has won.
//      Parameters:         none
//      Returns:            true if the board is full, false
//                          otherwise
bool Board::isFull() const
{
    for (int c = 0; c < columns; c++) {
        if (vBoard[0][c] == EMPTY) {
            return false;
        }
    }
    return true;
}


// Function resets the board by making each position EMPTY. This
// function can be used to reset the board after a game is over
// so that another game can be played before the program ends.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      every position on the board
//                          is marked as EMPTY, Sprites
//                          are reset to the EMPTY texture
void Board::makeEmpty()
{
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < columns; c++) {
            vBoard[r][c] = EMPTY;
            boardSprite[r][c].setTexture(empty);
        }
    }
}


// Function draws the game board to the passed SFML render window.
//      Parameters:         sf::RenderWindow& window: window to 
//                          draw to
//      Returns:            none
void Board::drawBoard(sf::RenderWindow& window)
{
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < columns; c++) {
            window.draw(boardSprite[r][c]);
        }
    }
}


// Function creates the board by resizing the 2D vectors for the
// board state and board sprites to the row and column values. This
// function also sets the position of the board sprites.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      vBoard vector and boardSprite vector
//                          are sized and set appropriately and
//                          board sprites are given positions
void Board::createBoard()
{
    // size 2D vBoard
    vBoard.resize(rows);
    for (int i = 0; i < rows; i++) {
        vBoard[i].resize(columns, EMPTY);
    }

    // size 2D boardSprites
    boardSprite.resize(rows);
    for (int i = 0; i < rows; i++) {
        boardSprite[i].resize(columns);
    }

    float horizontalPos = 0;        // horizontal position of sprite
    float verticalPos = 0;          // vertical position of sprite

    // set position and texture of board sprites
    for (int r = 0; r < rows; r++) {
        horizontalPos = 0;
        for (int c = 0; c < columns; c++) {
            boardSprite[r][c].setTexture(empty);
            boardSprite[r][c].setPosition(horizontalPos, verticalPos);
            horizontalPos += TEXTURE_WIDTH;
        }
        verticalPos += TEXTURE_HEIGHT;
    }
}


// Function checks if the passed board position created a win
// situation for the player represented by the passed piece in the
// horizontal direction.
//      Parameters:         int r: row of position to check
//                          int c: column of position to check
//                          piece p: player to check win
//      Returns:            true if a win situation is present,
//                          false if not. If passed board positon
//                          is invalid, a false is returned
bool Board::checkWinHorizontal(int r, int c, piece p) const
{
    int streak = 0;         // streak counter

    if (r < 0 || r >= rows || c < 0 || c >= columns) {
        std::cout << "Board::checkWinHorizontal: Error, invalid board position." << std::endl;
        return false;
    }

    // check left of (r, c) until piece different from p found
    for (int ic = c; ic >= 0; ic--) {
        if (ic == 0 && vBoard[r][ic] == p) {
            c = 0;
            break;
        }
        if (vBoard[r][ic] != p) {
            c = ic + 1;
            break;
        }
    }

    // check right until piece different from p found
    for (int ic = c; ic < columns; ic++) {
        if (vBoard[r][ic] != p) {
            break;
        }
        streak++;
    }

    return streak >= n;
}


// Function checks if the passed board position created a win
// situation for the player represented by the passed piece in the
// vertical direction.
//      Parameters:         int r: row of position to check
//                          int c: column of position to check
//                          piece p: player to check win
//      Returns:            true if a win situation is present,
//                          false if not. If passed board position
//                          is invalid, a false is returned.
bool Board::checkWinVertical(int r, int c, piece p) const
{
    int streak = 0;         // streak counter

    if (r < 0 || r >= rows || c < 0 || c >= columns) {
        std::cout << "Board::checkWinVertical: Error, invalid board location." << std::endl;
        return false;
    }

    // check above (r, c) until piece different from p found
    for (int ir = r; ir >= 0; ir--) {
        if (ir == 0 && vBoard[ir][c] == p) {
            r = 0;
            break;
        }
        if (vBoard[ir][c] != p) {
            r = ir + 1;
            break;
        }
    }

    // check below until piece different from p found
    for (int ir = r; ir < rows; ir++) {
        if (vBoard[ir][c] != p) {
            break;
        }
        streak++;
    }

    return streak >= n;
}


// Function checks if the passed board position created a win
// situation for the player represented by the passed piece in the
// positive diagonal direction.
//      Parameters:         int r: row of position to check
//                          int c: column of position to check
//                          piece p: player to check win
//      Returns:            true if a win situation is present,
//                          false if not. If passed board position
//                          is invalid, a false is returned
bool Board::checkWinPosDiag(int r, int c, piece p) const
{
    int streak = 0;         // streak counter

    if (r < 0 || r >= rows || c < 0 || c >= columns) {
        std::cout << "Board::checkWinPosDiag: Error, invalid board location." << std::endl;
        return false;
    }

    // check up and right until piece different from p found
    for (int ir = r, ic = c; ir >= 0 && ic < columns; ir--, ic++) {
        if (ir == 0 && vBoard[ir][ic] == p) {
            r = 0;
            c = ic;
            break;
        }
        if (ic == columns - 1 && vBoard[ir][ic] == p) {
            c = columns - 1;
            r = ir;
            break;
        }
        if (vBoard[ir][ic] != p) {
            r = ir + 1;
            c = ic - 1;
            break;
        }
    }

    // check down and left until piece different from p found
    for (int ir = r, ic = c; ir < rows && ic >= 0; ir++, ic--) {
        if (vBoard[ir][ic] != p) {
            break;
        }
        streak++;
    }

    return streak >= n;
}


// Function checks if the passed board position created a win
// situation for the player represented by the passed piece in the
// horizontal direction.
//      Parameters:         int r: row of position to check
//                          int c: column of position to check
//                          piece p: player to check win
//      Returns:            true if a win situation is present,
//                          false if not
bool Board::checkWinNegDiag(int r, int c, piece p) const
{
    int streak = 0;         // streak counter

    if (r < 0 || r >= rows || c < 0 || c >= columns) {
        std::cout << "Board:checkWinNegDiag: Error, invalid board location." << std::endl;
        return false;
    }

    // check up and left until piece different from p found
    for (int ir = r, ic = c; ir >= 0 && ic >= 0; ir--, ic--) {
        if (ir == 0 && vBoard[ir][ic] == p) {
            c = ic;
            r = 0;
            break;
        }
        if (ic == 0 && vBoard[ir][ic] == p) {
            c = 0;
            r = ir;
            break;
        }
        if (vBoard[ir][ic] != p) {
            r = ir + 1;
            c = ic + 1;
            break;
        }
    }

    // check down and right until piece different from p found
    for (int ir = r, ic = c; ir < rows && ic < columns; ir++, ic++) {
        if (vBoard[ir][ic] != p) {
            break;
        }
        streak++;
    }

    return streak >= n;
}
