// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  ConnectN.cpp
//
//  This file implements a class that plays a game of Connect N.
//  The class uses the Board class, the UserPlayer class, and
//  the ComputerPlayer class. The game allows the rows and
//  columns of the board as well as the N value to be customized.
//  The class contains functions to play a game of Connect N using
//  graphics using SFML.


#include <iostream>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "ConnectN.h"
#include "Board.h"


// Function is the default constructor for the Connect N game. The
// constructor sets the rows, columns, and N value for the board.
// Default values of m, r, and c are provided and are 4, 6, and 7
// respectively.
//      Parameters:         int m: integer to set as n value
//                          int r: integer to set as row value
//                          int c: integer to set as column value
//      Returns:            none
//      Postcondition:      board rows, columns, and n value set
//                          for game. user and computer colors
//                          are also set.
ConnectN::ConnectN(int m, int r, int c)
{
    setBoardSize(r, c);
    setN(m);
    user.setColor(RED);
    computer.setColor(BLACK);

    usersTurn = true;

    init();
}


// Function sets the N value for the board. Function calls Board's
// setN() function.
//      Parameters:         int m: value to set as n value
//      Returns:            true if value was set, false otherwise
//      Postcondition:      board's n value is set (if valid)
bool ConnectN::setN(int m)
{
    return board.setN(m);
}


// Function sets the board size for the board. Function calls the
// Board's setSzie() function.
//      Parameters:         int r: value to set as rows
//                          int c: value to set as columns
//      Returns:            true if values were set, false otherwise
//      Postcondition:      board's size set (if valid)
bool ConnectN::setBoardSize(int r, int c)
{
    return board.setSize(r, c);
}


// Function sets the computer's game strategy to offensive. Function
// calls the ComputerPlayer's setStrategy() function
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerStrategyOffensive()
{
    computer.setStrategy(OFFENSIVE);
}


// Function sets the computer's game strategy to defensive. Function
// calls the ComputerPlayer's setStrategy() function
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerStrategyDefensive()
{
    computer.setStrategy(DEFENSIVE);
}


// Function sets the computer's game strategy to balanced. Function
// calls the ComputerPlayer's setStrategy() function
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerStrategyBalanced()
{
    computer.setStrategy(BALANCED);
}


// Function sets the computer's game difficulty to easy. Function calls
// the ComputerPlayer's setDifficulty() function.
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerDifficultyEasy()
{
    computer.setDifficulty(EASY);
}


// Function sets the computer's game difficulty to medium. Function
// calls the ComputerPlayer's setDifficulty() function.
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerDifficultyMedium()
{
    computer.setDifficulty(MEDIUM);
}


// Function sets the computer's game difficulty to hard. Function calls
// the ComputerPlayer's setDifficulty() function.
//      Parameters:         none
//      Returns:            none
void ConnectN::setComputerDifficultyHard()
{
    computer.setDifficulty(HARD);
}


// Function plays a single game of Connect N. Function uses SFML to
// display a window with graphics representing the board. Function
// calls the private drawWindow() function to draw graphics to the
// window and calls the private takeTurns() functions to get the
// user's and computer's turns.
//      Parameters:         none
//      Returns:            none
void ConnectN::playGame()
{
    // initial error checking to ensure everything is in order 
    // before starting game.
    if (board.getN() > board.getRows()
        || board.getN() > board.getColumns()) {
        std::cout << "Eroror, could not start game because N value "
            << "is greater than rows and/or columns." << std::endl;
        return;
    }

    if (user.getColor() == EMPTY || computer.getColor() == EMPTY) {
        std::cout << "Error, could not start game because user and/or computer"
            << " color is not set." << std::endl;
        return;
    }

    if (user.getColor() == computer.getColor()) {
        std::cout << "Error, could not start game because both players "
            << "have same piece color." << std::endl;
        return;
    }

    // create and set window
    window.create(sf::VideoMode(static_cast<int>(WINDOW_WIDTH),
        static_cast<int>(WINDOW_HEIGHT)), "Connect N");
    window.setPosition(sf::Vector2i(0, 0));

    // set up the board to start
    initializeBoard();

    gameOver = false;       // game is not over
    userWon = true;         // assume user wins
    tieGame = false;        // assume game does not end in tie

    while (window.isOpen()) {
        sf::Event event;
        // process window's events
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
                gameOver = true;
            }

            if (event.type == sf::Event::KeyPressed
                && event.key.code == sf::Keyboard::Escape) {
                window.close();
                gameOver = true;
            }
        }

        // draw window
        drawWindow();

        // game logic
        if (userTurn()) {
            usersTurn = false;
            drawWindow();
            if (!gameOver) {
                computerTurn();
                usersTurn = true;
            }
        }

        if (gameOver) {
            // see if user wants to play again
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                while (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                    // wait for mouse button to be released...
                }

                gameOver = false;
                initializeBoard();
            }
        }
    }
}


// Function initializes the board to an empty state. This function
// could be used to reset the board after a game has ended to start
// a new game.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      board set to an empty state
void ConnectN::initializeBoard()
{
    board.makeEmpty();
    gameOver = false;
    userWon = true;
    tieGame = false;
    usersTurn = true;
}


// Function sets up the font, and text objects for the game.
// This function loads the font, and sets up the text objects.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      font loaded, text objects created
void ConnectN::init()
{
    // load from file
    if (!font.loadFromFile("resources/fonts/Tinos-BoldItalic.ttf")) {
        std::cout << "Error, unable to load font." << std::endl;
        return;
    }

    // set font for text objects
    winMsg.setFont(font);
    lossMsg.setFont(font);
    drawMsg.setFont(font);
    turnMsg.setFont(font);
    waitMsg.setFont(font);

    // set messages
    winMsg.setString("You Won! Click anywhere to play again.");
    lossMsg.setString("You Lost! Click anywhere to play again.");
    drawMsg.setString("Tie! Click anywhere to play again.");
    turnMsg.setString("Your turn! Click on a column to make your turn.");
    waitMsg.setString("Computer's turn! Please wait.");

    // get bounding rectangles (used to set origins)
    sf::FloatRect winRect = winMsg.getLocalBounds();
    sf::FloatRect lossRect = lossMsg.getLocalBounds();
    sf::FloatRect drawRect = drawMsg.getLocalBounds();
    sf::FloatRect turnRect = turnMsg.getLocalBounds();
    sf::FloatRect waitRect = waitMsg.getLocalBounds();

    // set text origins
    winMsg.setOrigin(winRect.width / 2, winRect.height / 2);
    lossMsg.setOrigin(lossRect.width / 2, lossRect.height / 2);
    drawMsg.setOrigin(drawRect.width / 2, drawRect.height / 2);
    turnMsg.setOrigin(turnRect.width / 2, turnRect.height / 2);
    waitMsg.setOrigin(waitRect.width / 2, waitRect.height / 2);

    // set positions
    winMsg.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT - OFFSET);
    lossMsg.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT - OFFSET);
    drawMsg.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT - OFFSET);
    turnMsg.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT - OFFSET);
    waitMsg.setPosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT - OFFSET);

    // set colors
    winMsg.setColor(sf::Color::Green);
    lossMsg.setColor(sf::Color::Red);
    drawMsg.setColor(sf::Color::Black);
    turnMsg.setColor(sf::Color::Black);
    waitMsg.setColor(sf::Color::Black);
}


// Function draws the appropriate graphics to the window.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      graphics drawn to window
void ConnectN::drawWindow()
{
    window.clear(sf::Color::White);

    // draw board sprites
    board.drawBoard(window);

    // if game is over, determine what message to draw
    if (gameOver) {
        if (tieGame) {
            window.draw(drawMsg);
        }
        else if (userWon) {
            window.draw(winMsg);
        }
        else {
            window.draw(lossMsg);
        }
    }
    else {
        // game is not over, display user's turn message
        if (usersTurn) {
            window.draw(turnMsg);
        }
        else {
            window.draw(waitMsg);
        }
    }

    window.display();
}


// Function waits for the user to take his/her turn by
// clicking on a row. Function also checks if game is
// over after user takes his/her turn due to user winning
// or board being full.
//      Parameters:         none
//      Returns:            true if user takes his/her
//                          turn, false otherwise
bool ConnectN::userTurn()
{
    if (!gameOver) {
        int userRow;                // user's row
        int userColumn;             // user's column

        bool turnTaken = false;     // if user takes turn

        // wait for mouse button click
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            while (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                // wait for mouse button to be released
            }
            // get mouse position
            sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
            if (mousePosition.y > 0 &&
                mousePosition.y <= board.getRows() * Board::TEXTURE_HEIGHT) {
                userColumn = user.getMove(mousePosition.x, mousePosition.y);

                // check for valid move
                if (board.validMove(userColumn)) {
                    userRow = board.placePiece(userColumn, user.getColor());
                    if (userRow != -1) {
                        turnTaken = true;
                    }
                }
            }
        }

        // if the user took their turn, did user win?
        if (turnTaken && board.checkWin(userRow, userColumn, user.getColor())) {
            gameOver = true;
            userWon = true;
        }
        else if (board.isFull()) {
            gameOver = true;
            tieGame = true;
        }

        return turnTaken;
    }

    return false;
}


// Function waits for the computer to take its turn.
// Function also checks if game is over after computer
// takes its turn due to computer winning or board being
// full.
//      Parameters:         none
//      Returns:            true if computer takes its
//                          turn, false otherwise
bool ConnectN::computerTurn()
{
    // if user did not win, take computer's turn
    if (!gameOver) {
        computer.setBoardState(board);

        int computerColumn = computer.getMove();    // computer's column
        int computerRow;                            // computer's row

        computerRow = board.placePiece(computerColumn, computer.getColor());

        // did computer win?
        if (board.checkWin(computerRow, computerColumn, computer.getColor())) {
            gameOver = true;
            userWon = false;
        }
        else if (board.isFull()) {
            gameOver = true;
            tieGame = true;
        }

        return true;
    }
    return false;
}
