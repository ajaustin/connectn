// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  UserPlayer.h
//
//  This file defines a class that represents a user player for
//  a game of Connect N. The class contains functions for getting
//  the user's move and setting and getting the user's piece color


#ifndef H_USERPLAYER
#define H_USERPLAYER

#include "Board.h"

class UserPlayer
{
public:
    // Function is the default constructor for the UserPlayer. The
    // function initializes the color of the user to EMPTY.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      player's color set to EMPTY
    UserPlayer();


    // Function gets the color of the user's piece.
    //      Parameters:         none
    //      Returns:            piece representing user's color
    piece getColor();


    // Function sets the user's color to the passed piece.
    //      Parameters:         piece p: color to set as
    //                          user's piece
    //      Returns:            none
    //      Postcondition:      user's color set
    void setColor(piece p);


    // Function gets the user's move based on the user's mouse
    // click coordinates.
    //      Parameters:         double x: x-value of coordinate
    //                          double y: y-value of coordinate
    //      Returns:            int move: column represented by
    //                          mouse click coordinates
    int getMove(double x, double y);

private:
    piece color;    // user's piece color
};

#endif
