// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  ComputerPlayer.h
//
//  This file defines a class that represents a computer player for
//  a game of Connect N. The class contains functions for getting
//  the computer's move and setting and getting the computer's piece
//  color


#ifndef H_COMPUTERPLAYER
#define H_COMPUTERPLAYER

#include "Board.h"

// difficult mode for computer. Determines how many
// moves computer is allowed to look ahead. Easy mode
// looks ahead 3 moves, Medium 4 moves, and Hard 5 moves.
enum mode { EASY, MEDIUM, HARD };

// determines strategy for computer. Deffensive places higher
// priority on blocking potential opponent threats, while
// offensive places priority on creating its own threats.
// Balanced is a compromise between the two.
enum strategy { DEFENSIVE, OFFENSIVE, BALANCED };

class ComputerPlayer
{
public:
    // Function is a default constructor for the ComputerPlayer. THe
    // function initializes the color of the computer's piece to EMPTY
    // and sets the computer's strategy and difficulty to defensive and
    // medium, respectively
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      computer's color set to EMPTY, difficulty
    //                          to medium, and strategy to defensive
    ComputerPlayer();


    // Function gets the color of the computer's piece.
    //      Parameters:         none
    //      Returns:            piece representing computer's color
    piece getColor();


    // Function sets the computer's color to the passed piece. Also
    // sets color of opponent's piece
    //      Parameters:         piece p: color to set as
    //                          computer's piece
    //      Returns:            none
    //      Postcondition:      computer's and oponent's colors set
    void setColor(piece c);


    // Function gets the computer's strategy.
    //      Parameters:         none
    //      Returns:            computer's strategy
    strategy getStrategy();


    // Function sets the computer's strategy to the passed strategy.
    //      Parameters:         strategy s: computer's strategy
    //      Returns:            none
    //      Postcondition:      computer's strategy set
    void setStrategy(strategy s);


    // Function gets the computer's difficulty mode.
    //      Parameters:         none
    //      Returns:            computer's difficulty mode
    mode getDifficulty();


    // Function sets the computer's difficulty to the passed
    // difficulty.
    //      Parameters:         mode m: difficulty of computer
    //      Returns:            none
    //      Postcondition:      computer's difficulty set
    void setDifficulty(mode m);


    // Function sets the computer's board state. This function
    // should be called at the beginning of the computer's turn.
    //      Parameters:         Board &board: board state
    //      Returns:            none
    //      Postcondition:      board state set
    void setBoardState(const Board& board);


    // Function gets the computer's move by searching for the best
    // move it can make given the current board state. The computer
    // looks ahead a specified number of moves and evaluates the
    // board once those moves have been made. THe board with the
    // best "score" is the move the comptuer will take.
    //      Parameters:         none
    //      Returns:            int move: computer's chosen move
    int getMove();

private:
    // Function uses the minimax algorithm to look ahead a certain number
    // of moves to help the computer determine which move to make. If
    // the maximum search depth has been reached or if there are no
    // possible moves left, evaluates the current board state.
    //      Parameters:     int currDepth: current search depth
    //                      bool maximizing: whether the current
    //                      player is the maximizing player
    //      Returns:        value of the best column move
    int minimax(int depth, bool maximizing, int alpha, int beta);


    // Function evaluates the current board state for the computer.
    // Evaluation function is:
    //      myCoefficient * myThreats - oppCoefficient * oppThreats
    // A threat is defined as having N-1 unblocked pieces in a row
    // allowing for gaps.
    //      Parameters:     none
    //      Returns:        evaluation score for board state
    int evaluateBoard();


    // Function counts the number of threats the player with
    // the passed piece has. Threats are defined as having
    // N - 1 unblocked pieces in a row, allowing for gaps.
    // For example, x_xx is counted as a horizontal threat.
    // Gaps are allowed at the beginning and end of threats,
    // so _xxx_ will count as two distinct threats. To count
    // as a threat, a player must be able to eventually place
    // a piece to complete the threat. For example, oxxx_ is
    // considered a valid threat, but oxxxo is not.
    //      Parameters:     piece p: color to count threats of
    //      Returns:        number of threats player has
    int countThreats(piece p);


    // Function checks if a horizontal threat starts at the
    // cell (r, c) for the player with piece p.
    //      Parameters:     int r: row of cell to start check
    //                      int c: column of cell to start check
    //                      piece p: player to check
    //      Returns:        1 if a threat starts at (r, c); 0 otherwise
    int checkThreatHorizontal(int r, int c, piece p);


    // Function checks if a vertical threat starts at the
    // cell (r, c) for the player with piece p.
    //      Parameters:     int r: row of cell to start check
    //                      int c: column of cell to start check
    //                      piece p: player to check
    //      Returns:        1 if a threat starts at (r, c); 0 otherwise
    int checkThreatVertical(int r, int c, piece p);


    // Function checks if a horizontal threat starts at the
    // cell (r, c) for the player with piece p.
    //      Parameters:     int r: row of cell to start check
    //                      int c: column of cell to start check
    //                      piece p: player to check
    //      Returns:        1 if a threat starts at (r, c); 0 otherwise
    int checkThreatPosDiag(int r, int c, piece p);


    // Function checks if a horizontal threat starts at the
    // cell (r, c) for the player with piece p.
    //      Parameters:     int r: row of cell to start check
    //                      int c: column of cell to start check
    //                      piece p: player to check
    //      Returns:        1 if a threat starts at (r, c); 0 otherwise
    int checkThreatNegDiag(int r, int c, piece p);


    // Function returns a shuffled list of all possible children of a given
    // state. List is shuffled to allow for variety in move selections in
    // the early parts of a game where the board is large and/or the number
    // of pieces in a row needed to win is large.
    //      Parameters:     const Board& state: the board state to generate
    //                      possible children of
    //      Returns:        a vector of ints representing the possible
    //                      children of a board state
    std::vector<int> getMoves(const Board& state);

    piece color;        // piece color for computer
    piece opponent;     // piece color for opponent
    Board state;        // board state for computer
    mode difficulty;    // difficulty of computer (based on depth)
    strategy strat;     // strategy of computer

    int depth;          // depth for minimax search

    const int EASY_DEPTH = 4;   // max depth in easy mode
    const int MED_DEPTH = 5;    // max depth in medium mode
    const int HARD_DEPTH = 6;   // max depth in hard mode

    int myThreatCoeff;          // weight placed on computer's threats
    int oppThreatCoeff;         // weight placed on opponent's threats
};

#endif
