// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  ConnectN.h
//
//  This file defines a class that plays a game of Connect N.
//  The class uses the Board class, the UserPlayer class, and
//  the ComputerPlayer class. The game allows the rows and
//  columns of the board as well as the N value to be customized.
//  The class contains functions to play a game of Connect N using
//  graphics using SFML.


#ifndef H_CONNECTN
#define H_CONNECTN

#include <string>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Board.h"
#include "ComputerPlayer.h"
#include "UserPlayer.h"

class ConnectN {
public:
    // Function is the default constructor for the Connect N game. The
    // constructor sets the rows, columns, and N value for the board.
    // Default values of m, r, and c are provided and are 4, 6, and 7
    // respectively.
    //      Parameters:         int m: integer to set as n value
    //                          int r: integer to set as row value
    //                          int c: integer to set as column value
    //      Returns:            none
    //      Postcondition:      board rows, columns, and n value set
    //                          for game. user and computer colors
    //                          are also set.
    ConnectN(int m = 4, int r = 6, int c = 7);


    // Function sets the N value for the board. Function calls Board's
    // setN() function.
    //      Parameters:         int m: value to set as n value
    //      Returns:            true if value was set, false otherwise
    //      Postcondition:      board's n value is set (if valid)
    bool setN(int m);


    // Function sets the board size for the board. Function calls the
    // Board's setSzie() function.
    //      Parameters:         int r: value to set as rows
    //                          int c: value to set as columns
    //      Returns:            true if values were set, false otherwise
    //      Postcondition:      board's size set (if valid)
    bool setBoardSize(int r, int c);


    // Function sets the computer's game strategy to offensive. Function
    // calls the ComputerPlayer's setStrategy() function
    //      Parameters:         none
    //      Returns:            none
    void setComputerStrategyOffensive();


    // Function sets the computer's game strategy to defensive. Function
    // calls the ComputerPlayer's setStrategy() function
    //      Parameters:         none
    //      Returns:            none
    void setComputerStrategyDefensive();


    // Function sets the computer's game strategy to balanced. Function
    // calls the ComputerPlayer's setStrategy() function
    //      Parameters:         none
    //      Returns:            none
    void setComputerStrategyBalanced();


    // Function sets the computer's game difficulty to easy. Function calls
    // the ComputerPlayer's setDifficulty() function.
    //      Parameters:         none
    //      Returns:            none
    void setComputerDifficultyEasy();


    // Function sets the computer's game difficulty to medium. Function
    // calls the ComputerPlayer's setDifficulty() function.
    //      Parameters:         none
    //      Returns:            none
    void setComputerDifficultyMedium();


    // Function sets the computer's game difficulty to hard. Function calls
    // the ComputerPlayer's setDifficulty() function.
    //      Parameters:         none
    //      Returns:            none
    void setComputerDifficultyHard();


    // Function plays a single game of Connect N. Function uses SFML to
    // display a window with graphics representing the board. Function
    // calls the private drawWindow() function to draw graphics to the
    // window and calls the private takeTurns() functions to get the
    // user's and computer's turns.
    //      Parameters:         none
    //      Returns:            none
    void playGame();

private:
    // Function initializes the board to an empty state. This function
    // could be used to reset the board after a game has ended to start
    // a new game.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      board set to an empty state
    void initializeBoard();


    // Function sets up the font, and text objects for the game.
    // This function loads the font, and sets up the text objects.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      font loaded, text objects created
    void init();

    // Function draws the appropriate graphics to the window.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      graphics drawn to window
    void drawWindow();

    // Function waits for the user to take his/her turn by
    // clicking on a row. Function also checks if game is
    // over after user takes his/her turn due to user winning
    // or board being full.
    //      Parameters:         none
    //      Returns:            true if user takes his/her
    //                          turn, false otherwise
    bool userTurn();

    // Function waits for the computer to take its turn.
    // Function also checks if game is over after computer
    // takes its turn due to computer winning or board being
    // full.
    //      Parameters:         none
    //      Returns:            true if computer takes its
    //                          turn, false otherwise
    bool computerTurn();

    Board board;                // represents the board for the game
    ComputerPlayer computer;    // represents the computer player
    UserPlayer user;            // represents the user player

    bool gameOver;              // whether the game is over
    bool userWon;               // whether the user has won
    bool tieGame;               // whether the game ends in a tie
    bool usersTurn;              // whether it is the user's turn

    sf::RenderWindow window;    // game's window
    sf::Font font;              // font for text objects
    sf::Text winMsg;            // win message to display if user wins
    sf::Text lossMsg;           // loss message to display if user losses
    sf::Text drawMsg;           // draw message to display if tie
    sf::Text turnMsg;           // tells user to take turn
    sf::Text waitMsg;           // tells the user to wait for computer

    const float WINDOW_WIDTH = 800;         // window width
    const float WINDOW_HEIGHT = 650;        // window height
    const float OFFSET = 30;                // offset from bottom of window to 
                                            // display text
};

#endif
