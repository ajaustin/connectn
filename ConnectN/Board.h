// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  Board.h
//
//  This file defines a class for the board in a Connect-N game.
//  The class contains functions for setting the size of the 
//  board and the N-value to connect. Class also performs
//  functions related to playing a game of Connect-N like
//  placing a piece, determining if a move is valid, and checking
//  for a win. The class also uses SFML to display graphics for the
//  board.


#ifndef H_BOARD
#define H_BOARD

#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>


// pieces for the board
//      EMPTY represents an empty space
//      RED represents space occupied by red piece
//      BLACK represents space occupied by black piece
//      INVALID is used only for error conditions
enum piece { EMPTY, RED, BLACK, INVALID };

class Board
{
public:
    static const int TEXTURE_WIDTH = 80;        // used for positioning of sprites
    static const int TEXTURE_HEIGHT = 60;       // used for positioning of sprites

    // Function is the default constructor. This function initializes the
    // class variables. rows, columns, and n are all set to defaults of
    // 6, 7, and 4, respectively. The class texture images are loaded from
    // their files.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      class variables initialized and textures
    //                          loaded
    Board();


    // Function sets the N value for the Connect-N board. The N value must
    // be set after the board size has been set. The N value is checked
    // to ensure that it is in the allowed range and then checked to ensure
    // that it is not greater than the number of rows and columns.
    //      Parameterss:        int m: value to set as the N value
    //      Returns:            true if N value was set, false if
    //                          an error occured (out of range,
    //                          larger than rows/columns, etc.)
    //      Precondition:       rows and columns set before this
    //                          function is called
    //      Postcondition:      N value set to passed value if it is
    //                          valid
    bool setN(int m);


    // Function sets the size of the board (i.e., the nuber of rows and
    // columns). The rows and columns values (if they are to be changed from
    // the defaults) must be set before the N value. The rows and columns
    // values are checked to ensure they are within the allowed range. This
    // function calls the private createBoard() function to create the board.
    //      Parameters:         int r: value to set as number of rows
    //                          int c: value to set as number of columns
    //      Returns:            true if rows and columns values were set,
    //                          false if an error occured (out of range,
    //                          etc.)
    //      Postcondition:      rows and columns values set to passed values
    //                          if they were valid
    bool setSize(int r, int c);


    // Function returns the N-value for the Connect-N board.
    //      Parameters:         none
    //      Returns:            n (int): the number of pieces that must be
    //                          connected for a win
    int getN() const;


    // Function returns the number of rows on the board.
    //      Parameters:         none
    //      Returns:            rows (int): the number of rows on the board
    int getRows() const;


    // Function returns the number of columns on the board.
    //      Parameters:         none
    //      Returns:            columns (int): the number of columns on the
    //                          board
    int getColumns() const;


    // Function returns (by reference) the number of rows and columns on the
    // board.
    //      Parameters:         int &r: the value to store the number of
    //                          rows
    //                          int &c: the value to store the number of
    //                          columns
    //      Returns:            rows (int) and columns (int) by reference
    void getSize(int& r, int&c) const;


    // Function returns the piece collor at the passed position.
    //      Parameters:         int r: row to get
    //                          int c: column to get
    //      Returns:            piece color at passed (r, c). returns
    //                          INVALID if position is invalid
    piece getCell(int r, int c) const;


    // Function checks if the passed (r, c) position created a win situation
    // for the player represented by the passed piece. This function calls
    // the private functions to check for a win horizontally, vertically,
    // and diagonally.
    //      Parameters:         int r: the value that represents the row
    //                          of the last move
    //                          int c: the value that represents the
    //                          column of the last move
    //                          piece p: the player to check for a win
    //      Returns:            true if the passed move created a win
    //                          situation for the player represented
    //                          by piece p; returns false if position
    //                          is invalid or if a win is checked for
    //                          an empty piece
    bool checkWin(int r, int c, piece p) const;


    // Function places a piece at the passed column. The function searches the
    // column from the bottom row up in order to place the passed piece. The
    // passed piece is placed at the first EMPTY position from the bottom of
    // the passed column. The row it was placed on is returned for convenience.
    // This function calls the public validMove() function to ensure that the
    // move is valid. If the move is invalid, a -1 is returned.
    //      Parameters:         int c: the column to place the piece in
    //                          piece p: the player making the move
    //      Returns:            r (int) row piece was placed in; -1 if
    //                          column represents an invalid move
    int placePiece(int c, piece p);


    // Function unplaces the piece at the top of the passed column. If
    // passed column is invalid or if passed column is empty, does nothing.
    //      Parameters:         int c: the column to remove a piece
    //                          from
    //      Returns:            none
    //      Postcondition:      topmost piece in passed column is
    //                          set back to empty.
    void unplacePiece(int c);


    // Function checks if a piece can be placed in the passed column. The column
    // is checked at row 0. If row 0 is EMPTY, than at least one row remains to
    // be filled in that column (since the column is filled from the bottom up).
    //      Parameters:         int c: the column to check if a move can be
    //                          made
    //      Returns:            true if a move can be made in that column,
    //                          false otherwise. If the passed column is
    //                          invalid, a false is returned.
    bool validMove(int c) const;


    // Function checks if the board is full. This function can be used to
    // determine if there is a tie since the game should end before the
    // board is full if a player has won.
    //      Parameters:         none
    //      Returns:            true if the board is full, false
    //                          otherwise
    bool isFull() const;


    // Function resets the board by making each position EMPTY. This
    // function can be used to reset the board after a game is over
    // so that another game can be played before the program ends.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      every position on the board
    //                          is marked as EMPTY, Sprites
    //                          are reset to the EMPTY texture
    void makeEmpty();


    // Function draws the game board to the passed SFML render window.
    //      Parameters:         sf::RenderWindow& window: window to 
    //                          draw to
    //      Returns:            none
    void drawBoard(sf::RenderWindow& window);

private:
    // Function creates the board by resizing the 2D vectors for the
    // board state and board sprites to the row and column values. This
    // function also sets the position of the board sprites.
    //      Parameters:         none
    //      Returns:            none
    //      Postcondition:      vBoard vector and boardSprite vector
    //                          are sized and set appropriately and
    //                          board sprites are given positions
    void createBoard();


    // Function checks if the passed board position created a win
    // situation for the player represented by the passed piece in the
    // horizontal direction.
    //      Parameters:         int r: row of position to check
    //                          int c: column of position to check
    //                          piece p: player to check win
    //      Returns:            true if a win situation is present,
    //                          false if not. If passed board positon
    //                          is invalid, a false is returned
    bool checkWinHorizontal(int r, int c, piece p) const;


    // Function checks if the passed board position created a win
    // situation for the player represented by the passed piece in the
    // vertical direction.
    //      Parameters:         int r: row of position to check
    //                          int c: column of position to check
    //                          piece p: player to check win
    //      Returns:            true if a win situation is present,
    //                          false if not. If passed board position
    //                          is invalid, a false is returned.
    bool checkWinVertical(int r, int c, piece p) const;


    // Function checks if the passed board position created a win
    // situation for the player represented by the passed piece in the
    // positive diagonal direction.
    //      Parameters:         int r: row of position to check
    //                          int c: column of position to check
    //                          piece p: player to check win
    //      Returns:            true if a win situation is present,
    //                          false if not. If passed board position
    //                          is invalid, a false is returned
    bool checkWinPosDiag(int r, int c, piece p) const;


    // Function checks if the passed board position created a win
    // situation for the player represented by the passed piece in the
    // horizontal direction.
    //      Parameters:         int r: row of position to check
    //                          int c: column of position to check
    //                          piece p: player to check win
    //      Returns:            true if a win situation is present,
    //                          false if not
    bool checkWinNegDiag(int r, int c, piece p) const;

    // 2D vector representing the state of the board
    std::vector<std::vector<piece>> vBoard;

    // 2D vector to store the board sprites
    std::vector <std::vector<sf::Sprite>> boardSprite;

    int rows;           // number of rows board has
    int columns;        // number of columns board has
    int n;              // number of pieces to connect in order to win

    sf::Texture empty;  // represents an empty space
    sf::Texture red;    // represents a space occupied by a red piece
    sf::Texture black;  // represents a space occupied by a black piece

    static const int MAX_ROWS = 10;     // limited by window size
    static const int MIN_ROWS = 4;      // to small to be fun
    static const int MAX_COLUMNS = 10;  // limited by window size
    static const int MIN_COLUMNS = 4;   // to small to be fun
    static const int MAX_N = 10;        // limited by max rows and columns
    static const int MIN_N = 3;         // to small to be fun
};

#endif
