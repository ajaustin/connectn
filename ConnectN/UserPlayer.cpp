// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  UserPlayer.cpp
//
//  This file implements a class that represents a user player for
//  a game of Connect N. The class contains functions for getting
//  the user's move and setting and getting the user's piece color


#include "UserPlayer.h"
#include "Board.h"


// Function is the default constructor for the UserPlayer. The
// function initializes the color of the user to EMPTY.
//      Parameters:         none
//      Returns:            none
//      Postcondition:      player's color set to EMPTY
UserPlayer::UserPlayer()
{
    color = EMPTY;
}


// Function gets the color of the user's piece.
//      Parameters:         none
//      Returns:            piece representing user's color
piece UserPlayer::getColor()
{
    return color;
}


// Function sets the user's color to the passed piece.
//      Parameters:         piece p: color to set as
//                          user's piece
//      Returns:            none
//      Postcondition:      user's color set
void UserPlayer::setColor(piece p)
{
    color = p;
}


// Function gets the user's move based on the user's mouse
// click coordinates.
//      Parameters:         double x: x-value of coordinate
//                          double y: y-value of coordinate
//      Returns:            int move: column represented by
//                          mouse click coordinates
int UserPlayer::getMove(double x, double y)
{
    return static_cast<int>(x / Board::TEXTURE_WIDTH);
}
