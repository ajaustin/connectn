// ******************************************************************
//  Amanda Austin
//  Connect N
// ******************************************************************


//  main.cpp
//
//  This file is the main entry point for the Connect N game.
//  This program processes the command line arguments and
//  if they were valid, plays a game of Connect N.


#include <iostream>
#include <string>
#include <sstream>

#include "ConnectN.h"

void displayUsage(char *argv[]);

int main(int argc, char *argv[])
{
    ConnectN game;            // game to play

    std::string nString = "";
    std::string rString = "";
    std::string cString = "";

    std::stringstream nConvert;
    std::stringstream rConvert;
    std::stringstream cConvert;

    bool strategySet = false;
    bool difficultySet = false;

    int nValue = 4;
    int rValue = 6;
    int cValue = 7;

    // if no command line args (besides program name), play with defaults
    if (argc == 1) {
        game.playGame();
        return 0;
    }

    // check arg count
    if (argc > 9) {
        std::cout << "Error, invalid command line arguments." << std::endl;
        displayUsage(argv);
        return -1;
    }

    // parse command line
    for (int i = 1; i < argc; i++) {
        if (std::string(argv[i]) == "-help") {
            displayUsage(argv);
            return 0;
        }
        else if (std::string(argv[i]) == "-e") {
            if (!difficultySet) {
                difficultySet = true;
                game.setComputerDifficultyEasy();
            }
            else {
                std::cout << "Error, only one difficulty setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-m") {
            if (!difficultySet) {
                difficultySet = true;
                game.setComputerDifficultyMedium();
            }
            else {
                std::cout << "Error, only one difficulty setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-h") {
            if (!difficultySet) {
                difficultySet = true;
                game.setComputerDifficultyHard();
            }
            else {
                std::cout << "Error, only one difficulty setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-o") {
            if (!strategySet) {
                strategySet = true;
                game.setComputerStrategyOffensive();
            }
            else {
                std::cout << "Error, only one strategy setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-d") {
            if (!strategySet) {
                strategySet = true;
                game.setComputerStrategyDefensive();
            }
            else {
                std::cout << "Error, only one strategy setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-b") {
            if (!strategySet) {
                strategySet = true;
                game.setComputerStrategyBalanced();
            }
            else {
                std::cout << "Error, only one strategy setting allowed." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-n") {
            if (i != argc - 1) {
                i++;
                nString = argv[i];
            }
            else {
                std::cout << "Error, invalid command line arguments." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-r") {
            if (i != argc - 1) {
                i++;
                rString = argv[i];
            }
            else {
                std::cout << "Error, invalid command line arguments." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else if (std::string(argv[i]) == "-c") {
            if (i != argc - 1) {
                i++;
                cString = argv[i];
            }
            else {
                std::cout << "Error, invalid command line arguments." << std::endl;
                displayUsage(argv);
                return -1;
            }
        }
        else {
            std::cout << "Error, invalid command line arguments." << std::endl;
            return -1;
        }
    }

    // check for valid r, c, and n values, if any were entered
    if (nString != "") {
        nConvert << nString;
        nConvert >> nValue;
        if (nConvert.fail()) {
            std::cout << "Error, N value must be integer." << std::endl;
            return -1;
        }
    }

    if (rString != "") {
        rConvert << rString;
        rConvert >> rValue;
        if (rConvert.fail()) {
            std::cout << "Error, r value must be integer." << std::endl;
            return -1;
        }
    }

    if (cString != "") {
        cConvert << cString;
        cConvert >> cValue;
        if (cConvert.fail()) {
            std::cout << "Error, c value must be integer." << std::endl;
            return -1;
        }
    }

    // set r, c, and n values
    if (!game.setBoardSize(rValue, cValue)) {
        return -1;
    }
    if (!game.setN(nValue)) {
        return -1;
    }

    // command line processed successfully, play game
    game.playGame();

    return 0;
}


void displayUsage(char *argv[])
{
    std::cout << "Usage: " << argv[0] << " option(s)" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "\t -help \t\t Display this usage message" << std::endl;
    std::cout << "\t -e \t\t Set computer difficulty to easy" << std::endl;
    std::cout << "\t -m \t\t Set computer difficulty to medium" << std::endl;
    std::cout << "\t -h \t\t Set computer difficulty to hard" << std::endl;
    std::cout << "\t -o \t\t Set computer strategy to offensive" << std::endl;
    std::cout << "\t -d \t\t Set computer strategy to defensive" << std::endl;
    std::cout << "\t -b \t\t Set computer strategy to balanced" << std::endl;
    std::cout << "\t -r ROWS \t Specify number of rows" << std::endl;
    std::cout << "\t -c COLUMNS \t Specify number of columns" << std::endl;
    std::cout << "\t -n N \t\t Specify N value" << std::endl;
    std::cout << std::endl;
}
