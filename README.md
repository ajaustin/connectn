Repository Imported from https://github.com/aj-austin/ConnectN

# ConnectN
Connect N is a generalized version of the Connect 4 game. Users have the option of setting the number of rows and columns of the board, as well as setting the number of pieces that must be connected for a win. The board is displayed using graphics via [SFML](http://www.sfml-dev.org/).

## Gameplay
The user plays against a computer AI that selects its move via the minimax algorithm with alpha-beta pruning. The user can specify the difficulty and strategy of the computer opponent.

#### Difficulty
The difficulty of the computer AI is determined by how many moves the computer is allowed to look ahead when choosing its move. In easy mode, this is limited to three (3) moves. In medium mode, the computer looks ahead four (4) moves. In hard mode, the computer looks ahead five (5) moves.

#### Strategy
The computer has three strategy options: offensive, defensive, and balanced. In offensive mode, the computer prioritizes making long chains of its own, while in defensive mode, the computer prioritizes preventing the user from making long chains. Balanced mode places an equal priority in creating long chains of its own and blocking the user's long chains.

## Running Connect N
Connect N provides several command-line options for running the game. If no command line options are specified, the default options of a 6x7 board with 4 pieces in a row needed to win and the computer in medium difficulty and defensive strategy are selected. If command line arguments are entered to choose the computer's difficulty and/or strategy, only one option from each may be chosen.

The command line arguments are as follows:

| Option            | Description                                              |
| ----------------- | -------------------------------------------------------- |
| `-r <number>`     | Specifies the number of rows on the board                |
| `-c <number>`     | Specifies the number of columns on the board             |
| `-n <number>`     | Specifies the number of pieces in a row needed to win    |
| `-e`              | Sets the computer player in easy mode                    |
| `-m`              | Sets the computer player in medium mode                  |
| `-h`              | Sets the computer player in hard mode                    |
| `-o`              | Sets the computer player's strategy to offensive         |
| `-d`              | Sets the computer player's strategy to defensive         |
| `-b`              | Sets the computer player's strategy to balanced          |