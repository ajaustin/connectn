# Amanda Austin
# Makefile for ConnectN

all: connectN

connectN: main.o Board.o ComputerPlayer.o ConnectN.o UserPlayer.o
	g++ --std=c++0x -Wall -Wpedantic -o connectN main.o Board.o ComputerPlayer.o ConnectN.o UserPlayer.o -lsfml-graphics -lsfml-window -lsfml-system

main.o: main.cpp ConnectN.h ConnectN.o
	g++ --std=c++0x -c -g -Wall -Wpedantic main.cpp

Board.o: Board.cpp Board.h
	g++ --std=c++0x -c -g -Wall -Wpedantic -lsfml-graphics Board.cpp

ComputerPlayer.o: ComputerPlayer.cpp ComputerPlayer.h Board.h Board.o
	g++ --std=c++0x -c -g -Wall -Wpedantic ComputerPlayer.cpp

ConnectN.o: ConnectN.cpp ConnectN.h Board.o Board.h ComputerPlayer.o ComputerPlayer.cpp UserPlayer.o UserPlayer.cpp
	g++ --std=c++0x -c -g -Wall -Wpedantic -lsfml-graphics -lsfml-window -lsfml-system ConnectN.cpp

UserPlayer.o: UserPlayer.cpp UserPlayer.h Board.h Board.o
	g++ --std=c++0x -c -g -Wall -Wpedantic UserPlayer.cpp


